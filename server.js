const express = require('express');
const server = express();
const mongoose = require('mongoose');
const config = require('./config');
const api = require('./controllers/apiController');

// Route
server.use('/', express.static(`${__dirname}/public`));

// Database Configuration
mongoose.connect(config.database());

// API Controller
api(server);

// Server listening port
server.listen(process.env.PORT || 3000);