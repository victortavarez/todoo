const User = require('../models/userModel');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const uniqid = require('uniqid');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const jwt_auth = require('express-jwt');

module.exports = function(server){

    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(cookieParser());

    // New User

    server.post('/users/register', (req, res) => {

        jwt.verify(req.body.token, 'RESTFULAPIs', (err, decoded) => {

            User.findOne({ username: decoded.uname }, (err, user) => {

                if (err) res.json({ error: err });

                if (!user) {

                    let hash_pass = bcrypt.hashSync(decoded.pass, 10);

                    User.create({

                        username: decoded.uname,
                        password: hash_pass,
                        email: decoded.email,
                        todos: []

                    }, (err, user) => {

                        res.json({ token: jwt.sign({ username: user.username, email: user.email, id: user.id, todos: user.todos }, 'RESTFULAPIs') });

                    });

                } else {

                    res.json({ token: jwt.sign({ error: 'Username already exists.' }, 'RESTFULAPIs') });

                };

            });

        });

    });

    // Login User

    server.post('/users/login', (req, res) => {

        jwt.verify(req.body.token, 'RESTFULAPIs', (err, decoded) => {

            if (err) throw err;

            User.findOne({ username: decoded.uname }, (err, user) => {

                if (err) return res.json({ error: err });

                if (!user) return res.json({ token: jwt.sign({ error: { username: 'Incorrect username' } }, 'RESTFULAPIs') });

                if (!bcrypt.compareSync(decoded.pass, user.password)) {

                    return res.json({ token: jwt.sign({ error: { password: 'Incorrect password' } }, 'RESTFULAPIs') });

                }

                return res.json({ token: jwt.sign({ username: user.username, email: user.email, id: user.id, todos: user.todos }, 'RESTFULAPIs') });

            });

        });

    });

    // Create a new task.

    server.post('/tasks/create/', (req, res) => {

        jwt.verify(req.body.token, 'RESTFULAPIs', (err, decoded) => {

            User.findByIdAndUpdate({ _id: JSON.parse(req.cookies.id) }, {
                $push: {
                    todos: {
                        name: decoded.name,
                        details: decoded.details,
                        date: decoded.date,
                        time: decoded.time,
                        category: decoded.category,
                        highPriority: decoded.priority,
                        completed: decoded.completed,
                        hasAttachement: decoded.hasAttachment,
                        attachment: decoded.attachment,
                        id: uniqid()
                    }
                }
            }, (err, user) => {

                if (err) throw err;

                if (!user) return res.json({ token: jwt.sign({ error: 'User tasks could not be updated.' }, 'RESTFULAPIs') });

                User.findById({ _id: JSON.parse(req.cookies.id) }, (err, user) => {

                    if (err) throw err;

                    if (!user) return res.json({ token: jwt.sign({ error: 'User tasks could not be located.' }, 'RESTFULAPIs') });

                    return res.json({ token: jwt.sign({ username: user.username, email: user.email, id: user.id, todos: user.todos }, 'RESTFULAPIs') });

                });

            });

        });

    });


    // Get Todos By Username

    // server.get('/api/todos/:username', (req, res) => {
    //     Todos.find({ username: req.params.username }, (err, todos) => {
    //         err ? res.send(err) : res.send(todos);
    //     });
    // });

    // // Get Todo By ID

    // server.get('/api/todo/:id', (req, res) => {
    //     Todos.findById({ _id: req.params.id }, (err, todo) => {
    //         err ? res.send(err) : res.send(todo);
    //     });
    // });

    // // Create OR Update Todo

    // server.post('/api/todo', (req, res) => {
    //     if(req.body.id){
    //         Todos.findByIdAndUpdate(req.body.id, {
    //             todo: req.body.todo,
    //             isDone: req.body.isDone,
    //             hasAttachement: req.body.hasAttachement
    //         },
    //             (err, todo) => err ? res.send(err) : res.send('Success'));
    //     } else {
    //         let newTodo = Todos({
    //             username: 'test',
    //             todo: req.body.todo,
    //             isDone: req.body.isDone,
    //             hasAttachement: req.body.hasAttachement
    //         });

    //         newTodo.save((err) => {
    //             err ? res.send(err) : res.send('Success');
    //         });
    //     }
    // });

    // // Delete Todo

    // server.delete('/api/todo', (req, res) => {
    //     Todos.findByIdAndRemove(req.body.id, (err) => {
    //         err ? res.send(err) : res.send('Success');
    //     });
    // });
}