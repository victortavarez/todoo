const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const todo = new Schema({
    name: String,
    details: String,
    date: Date,
    time: Date,
    category: Number,
    highPriority: Boolean,
    completed: Boolean,
    hasAttachement: Boolean,
    attachment: Buffer,
    id: String
});

const user = new Schema({
    username: String,
    password: String,
    email: String,
    todos: [todo]
});

module.exports = mongoose.model('User', user);