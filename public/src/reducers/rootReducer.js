import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { createUserReducer, fetchUserReducer, submitTaskReducer, openMenuReducer, openCreateReducer} from './reducers';


const rootReducer = combineReducers({
    creatingUser: createUserReducer,
    fetchingUser: fetchUserReducer,
    submittingTask: submitTaskReducer,
    menu: openMenuReducer,
    create: openCreateReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;