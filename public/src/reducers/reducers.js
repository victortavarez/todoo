import { CREATING_USER, CREATED_USER, CREATING_USER_ERROR, FETCHING_USER, FETCHED_USER, FETCHING_USER_ERROR, SUBMITTING_TASK, SUBMITTING_TASK_ERROR, SUBMITTED_TASK, OPEN_MENU, CLOSE_MENU, OPEN_CREATE, CLOSE_CREATE } from '../actions/actions';

export function createUserReducer(state = { isCreating: false, payload: {},  error: null }, action) {

    switch (action.type) {
        case CREATING_USER:
            return Object.assign({}, state, { isCreating: action.isCreating, payload: action.payload, error: action.error });
        case CREATED_USER:
            return Object.assign({}, state, { isCreating: action.isCreating, payload: action.payload, error: action.error });
        case CREATING_USER_ERROR:
            return Object.assign({}, state, { isCreating: action.isCreating, payload: action.payload, error: action.error });
        default:
            return state;
    }

};


export function fetchUserReducer(state = { isFetching: false, payload: {}, error: null }, action) {

    switch (action.type) {
        case FETCHING_USER:
            return Object.assign({}, state, { isFetching: action.isFetching, payload: action.payload, error: action.error });
        case FETCHED_USER:
            return Object.assign({}, state, { isFetching: action.isFetching, payload: action.payload, error: action.error });
        case FETCHING_USER_ERROR:
            return Object.assign({}, state, { isFetching: action.isFetching, payload: action.payload, error: action.error });
        default:
            return state;
    }

};


export function submitTaskReducer(state = { isSubmitting: false, payload: {}, error: null }, action) {
    switch (action.type) {
        case SUBMITTING_TASK:
            return Object.assign({}, state, { isSubmitting: action.isSubmitting, payload: action.payload, error: action.error });
        case SUBMITTED_TASK:
            return Object.assign({}, state, { isSubmitting: action.isSubmitting, payload: action.payload, error: action.error });
        case SUBMITTING_TASK_ERROR:
            return Object.assign({}, state, { isSubmitting: action.isSubmitting, payload: action.payload, error: action.error });
        default:
            return state;
    }
};


export function openMenuReducer(state = { open: false }, action){

    switch (action.type) {
        case OPEN_MENU:
            return Object.assign({}, state, { open: action.open });
        case CLOSE_MENU:
            return Object.assign({}, state, { open: action.open });
        default:
            return state;
    }

};


export function openCreateReducer(state = { open: false }, action) {

    switch (action.type) {
        case OPEN_CREATE:
            return Object.assign({}, state, { open: action.open });
        case CLOSE_CREATE:
            return Object.assign({}, state, { open: action.open });
        default:
            return state;
    }

};