function Validate(){};

Validate.prototype.username = function(uname){

    // Username Validation

    if (uname === '') {

        return 'This field is required';

    } else if (uname.length < 5) {

        return 'Username must be at least 5 characters long.';

    } else if (uname.match(/[\W]/g)) {

        return 'Username cannot have spaces or special characters.';

    }

    return;

};

Validate.prototype.email = function(email){
    
    // E-mail Validation

    if(email === ''){

        return 'This field is required';

    } else if (!email.match(/[@]/g) || email.match(/[!#$%^&*()></\/,`~=+|\]\[{}]/g)){

        return 'A valid e-mail address is required.';

    }

    return;

};

Validate.prototype.password = function(pass){

    // Password Validation

    if (pass === '') {

        return 'This field is required';

    } else if (pass.length < 10 ) {

        return 'Password must be at least 10 characters long.';

    } else if (!pass.match(/[0-9]/g)) {

        return 'Password requires at least (1) number';

    } else if (!pass.match(/[A-Z]/g)) {

        return 'Password requires at least (1) capital letter';

    } else if (pass.match(/[@#$%^&*()></\/,`~=+|\]\[{}]/g)){

        return 'Password cannot contain any special characters';

    }

    return;

};


Validate.prototype.passwords = function(pass, confirmpass){

    if (confirmpass === '') {

        return 'This field is required';

    } else if (pass !== confirmpass) {

        return 'Passwords does not match';

    }

    return;

}

export default new Validate();