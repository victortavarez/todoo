function _(){
    
    this.date = new Date();

};

_.prototype.getDayWithOrdinal = function (day) {
    let d = this.date.getDate();

    if(day){
        d = day.getDate();
    }


    if (d === 1 || d === 21 || d === 31) {

        return `${d}st`;

    } else if (d === 2 || d === 22) {

        return `${d}nd`;

    } else if (d === 3 || d === 23){

        return `${d}rd`;

    } else if (d >= 4 && d < 31) {

        return `${d}th`;

    }
}

_.prototype.getDayByName = function (d) {
    let weekday = this.date.getDay();
    if(d){
        weekday = d.getDay();
    }
    let week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return week[weekday];
}

_.prototype.getMonthByName = function (m) {
    let month = this.date.getMonth();
    if (m) {
        month = m.getMonth();
    }
    let year = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return year[month];
}

export default new _();

