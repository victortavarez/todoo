import Axios from 'axios';
import jwt_decode from 'jwt-decode';
import KJUR from 'jsrsasign';
import Cookies from 'cookies-js';
export const CREATING_USER = 'CREATING USER';
export const CREATED_USER = 'CREATED USER';
export const CREATING_USER_ERROR = 'CREATING USER ERROR';
export const FETCHING_USER = 'FETCHING USER';
export const FETCHED_USER = 'FETCHED USER';
export const FETCHING_USER_ERROR = 'FETCHING USER ERROR';
export const SUBMITTING_TASK = 'SUBMITTING TASK';
export const SUBMITTING_TASK_ERROR = 'SUBMITTING TASK ERROR';
export const SUBMITTED_TASK = 'SUBMITTED TASK';
export const OPEN_MENU = 'OPEN_MENU';
export const CLOSE_MENU = 'CLOSE_MENU';
export const OPEN_CREATE = 'OPEN_CREATE';
export const CLOSE_CREATE = 'CLOSE_CREATE';

export function createUser(uname, email, pass){

    return dispatch => {

        dispatch({ type: CREATING_USER, isCreating: true,  payload: {}, error: null });

        const header = { alg: "HS256", typ: "JWT" };
        const payload = { uname, email, pass };

        return Axios.post('/users/register/', {
            
            token: KJUR.jws.JWS.sign("HS256", header, payload, { utf8: "RESTFULAPIs" })

        }).then((res) => {

            if (KJUR.jws.JWS.verifyJWT(res.data.token, 'RESTFULAPIs', { alg: ['HS256'] })) {

                let data = jwt_decode(res.data.token);

                if (data.error) {

                    dispatch({ type: CREATING_USER_ERROR, isCreating: false, payload: {}, error: data.error });

                } else {

                    Cookies.set('authenticated', 'true', { expires: 604800 });
                    Cookies.set('id', JSON.stringify(data.id));
                    localStorage.setItem('username', JSON.stringify(data.username));
                    localStorage.setItem('email', JSON.stringify(data.email));
                    localStorage.setItem('todos', JSON.stringify(data.todos));


                    dispatch({ type: CREATED_USER, isCreating: false, payload: data, error: null });

                }

            }

        });

    };

};

export function fetchUser(uname, pass){

    return dispatch => {

        dispatch({ type: FETCHING_USER, isFetching: true, payload: {}, error: null });

        const header = { alg: "HS256", typ: "JWT" };
        const payload = { uname, pass };

        Axios.post('/users/login/', {

            token: KJUR.jws.JWS.sign("HS256", header, payload, {utf8: "RESTFULAPIs"})

        }).then((res) => {

            if (KJUR.jws.JWS.verifyJWT(res.data.token, 'RESTFULAPIs', { alg: ['HS256']})) {

                let data = jwt_decode(res.data.token);

                if (data.error) {

                    dispatch({ type: FETCHING_USER_ERROR, isFetching: false, payload: {}, error: data.error });

                } else {

                    Cookies.set('authenticated', 'true', { expires: 604800 });
                    Cookies.set('id', JSON.stringify(data.id));
                    localStorage.setItem('username', JSON.stringify(data.username));
                    localStorage.setItem('email', JSON.stringify(data.email));
                    localStorage.setItem('todos', JSON.stringify(data.todos));

                    dispatch({ type: FETCHED_USER, isFetching: false, payload: data, error: null });

                }

            }

        });

    };

};


export function submitTask(name, details, date, time, category, priority, completed, hasAttachment, attachment){

    return dispatch => {

        dispatch({ type: SUBMITTING_TASK, isSubmitting: true, payload: {}, error: null });

        const header = { alg: 'HS256', typ: 'JWT'};
        const payload = { name, details, date, time, category, priority, completed, hasAttachment, attachment };

        Axios.post('/tasks/create/', { 

            token: KJUR.jws.JWS.sign("HS256", header, payload, { utf8: "RESTFULAPIs" })

        }).then((res) => {
            
            if (KJUR.jws.JWS.verifyJWT(res.data.token, 'RESTFULAPIs', { alg: ['HS256'] })) {

                let data = jwt_decode(res.data.token);

                if (data.error) {

                    dispatch({ type: SUBMITTING_TASK_ERROR, isSubmitting: false, payload: {}, error: data.error });

                } else {

                    localStorage.setItem('todos', JSON.stringify(data.todos));

                    dispatch({ type: SUBMITTED_TASK, isSubmitting: false, payload: data, error: null });

                }

            }

        });

    };

};


export function openMenu(bool){

    return dispatch => {
        
        bool ? dispatch({ type: OPEN_MENU, open: bool }) : dispatch({ type: CLOSE_MENU, open: bool });
        
    };

};


export function openCreate(bool) {

    return dispatch => {

        bool ? dispatch({ type: OPEN_CREATE, open: bool }) : dispatch({ type: CLOSE_CREATE, open: bool });

    };

};



