import React, { Component } from 'react';
import { connect } from 'react-redux';
import { dispatch } from 'redux';
import styled from 'styled-components';
import DialogModal from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import {Tabs, Tab} from 'material-ui/Tabs';
import SigninIcon from 'material-ui/svg-icons/action/account-circle';
import SignupIcon from 'material-ui/svg-icons/action/assignment';
import Button from 'material-ui/RaisedButton';
import UsernameIcon from 'material-ui/svg-icons/action/assignment-ind';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import PassIcon from 'material-ui/svg-icons/communication/vpn-key';
import VerifyIcon from 'material-ui/svg-icons/action/verified-user';
import Spinner from '../components/spinner';
import Validate from '../validate';
import { createUser, fetchUser } from '../actions/actions';
import Cookies from 'cookies-js';


const InputContainer = styled.div`
    width: 100%;
    height: auto;
    white-space: nowrap;
`;

const IconWrapper = styled.div`
    display: inline-block;
    vertical-align: top;
    width: 64px;
    height: 82px;
`;

class NewAccountLoginModal extends Component {
    constructor(props){
        super(props);

        this.state = {
            open: true,
            selectedTab: 1,
            new_username: '',
            new_username_error: '',
            new_email: '',
            new_email_error: '',
            new_password: '',
            new_password_error: '',
            confirmed_password: '',
            confirmed_password_error: '',
            username: '',
            username_error: '',
            password: '',
            password_error: '',
            loading: true
        };

        this.dividerStyles = {
            backgroundColor: 'transparent'
        };

        this.textFieldStyles = {
            display: 'inline-block',
            margin: '5px 0',
            width: 'calc(100% - 64px)'
        };

        this.iconStyles = {
            position: 'relative',
            top: '41px',
            width: '32px',
            height: '32px'
        };

        this.creatingUser = this.creatingUser.bind(this);
        this.fetchingUser = this.fetchingUser.bind(this);
    };


    // Will evaluate if user has logged in and persist session if authenticated.

    componentDidMount() {
        if (Cookies.get('authenticated')) {
            this.setState({ open: false });
        }
    };

    
    // Will evaluate if modal should be open or closed based on if a user was created or fetched.

    componentWillReceiveProps(nextProps){

        // If user already exists in database, error response is rendered to user.

        if(nextProps.creatingUser.error){

            this.setState({ 
                new_username_error: nextProps.creatingUser.error, 
                selectedTab: 0 
            });

            setTimeout(() => this.setState({ new_username_error: ''}), 2000);

        };

        // If username or password are incorrect, error response is rendered to user.
        
        if(nextProps.fetchingUser.error){

            this.setState({ 
                username_error: nextProps.fetchingUser.error.username || '', 
                password_error: nextProps.fetchingUser.error.password || ''
            });

            setTimeout(() => this.setState({ username_error: '', password_error: '' }), 2000);

        };

        // If no errors, user is logged in, inputs are reset and modal is closed.      
        
        if(!nextProps.creatingUser.error && !nextProps.fetchingUser.error){

            this.setState({ 
                new_username: '', 
                new_email: '', 
                new_password: '', 
                confirmed_password: '', 
                username: '', 
                password: '' 
            });

            nextProps.creatingUser.isCreating || nextProps.fetchingUser.isFetching ? null : (this.setState({ open: false }));

        };

    };

    // Function which validates newly created users. Submits form values if inputs are valid.

    creatingUser(e){
        e.preventDefault();

        /* Validate.username, Validate.email, Validate.password
            (1) Accepts the input value as an argument. 
            (2) Returns an error if the input value is not valid or missing.
        */

        /* Validate.passwords (optional)
            (1) Accepts the input values of password and confirm password as arguments.
            (2) Returns an error if the values do not match or missing.
        */

        let usernameError = Validate.username(this.state.new_username);
        let emailError = Validate.email(this.state.new_email);
        let passwordError = Validate.password(this.state.new_password);
        let confirmError = Validate.passwords(this.state.new_password, this.state.confirmed_password);

        if (usernameError) {

            this.setState({ new_username_error: usernameError });
            setTimeout(() => this.setState({ new_username_error: '' }), 2000);

        }
       
        if (emailError) {

            this.setState({ new_email_error: emailError });
            setTimeout(() => this.setState({ new_email_error: '' }), 2000);

        }

        if (passwordError) {

            this.setState({ new_password_error: passwordError });
            setTimeout(() => this.setState({ new_password_error: '' }), 2000);

        }

        if(confirmError){

            this.setState({ confirmed_password_error: confirmError });
            setTimeout(() => this.setState({ confirmed_password_error: '' }), 2000);

        }

        if (!usernameError && !emailError && !passwordError && !confirmError) {

            this.props.createUser(this.state.new_username, this.state.new_email, this.state.new_password);

        }

    };

    fetchingUser(e){
        e.preventDefault();

        let usernameError = Validate.username(this.state.username);
        let passwordError = Validate.password(this.state.password);

        if (usernameError) {

            this.setState({ username_error: usernameError });
            setTimeout(() => this.setState({ username_error: '' }), 2000);

        }

        if (passwordError) {

            this.setState({ password_error: passwordError });
            setTimeout(() => this.setState({ password_error: '' }), 2000);

        }

        if(!usernameError && !passwordError){

            this.props.fetchUser(this.state.username, this.state.password);

        };

    };


    render() {

        return (

            <DialogModal
                modal={true}
                open={this.state.open}
                overlayStyle={{ backgroundColor: '#1D2733' }}
            >

                { this.props.creatingUser.isCreating || this.props.fetchingUser.isFetching ? (
                    
                    <Spinner/>
                    
                ) : (

                        <Tabs initialSelectedIndex={this.state.selectedTab}>
                            <Tab label={'New account'} icon={<SignupIcon />}>
                                <form onSubmit={this.creatingUser}>
                                    <InputContainer>
                                        <IconWrapper>
                                            <UsernameIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            name={'new_username'}
                                            id={'new_username'}
                                            type={'text'}
                                            floatingLabelText={'Username'}
                                            onChange={(e) => this.setState({ new_username: e.target.value })}
                                            value={this.state.new_username}
                                            errorText={this.state.new_username_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <InputContainer>
                                        <IconWrapper>
                                            <EmailIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            id={'new_email'}
                                            type={'text'}
                                            floatingLabelText={'E-mail'}
                                            onChange={(e) => this.setState({ new_email: e.target.value })}
                                            value={this.state.new_email}
                                            fullWidth={true}
                                            errorText={this.state.new_email_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <InputContainer>
                                        <IconWrapper>
                                            <PassIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            id={'new_password'}
                                            type={'password'}
                                            floatingLabelText={'Password'}
                                            onChange={(e) => this.setState({ new_password: e.target.value })}
                                            value={this.state.new_password}
                                            fullWidth={true}
                                            errorText={this.state.new_password_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <InputContainer>
                                        <IconWrapper>
                                            <VerifyIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            id={'confirmed_password'}
                                            type={'password'}
                                            floatingLabelText={'Confirm Password'}
                                            onChange={(e) => this.setState({ confirmed_password: e.target.value })}
                                            value={this.state.confirmed_password}
                                            fullWidth={true}
                                            errorText={this.state.confirmed_password_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <Button
                                        type={'Submit'}
                                        label={'Submit'}
                                        fullWidth={true}
                                        style={{ marginTop: 20 }}
                                    />
                                </form>
                            </Tab>
                            <Tab label={'Sign in'} icon={<SigninIcon />}>
                                <form onSubmit={this.fetchingUser}>
                                    <InputContainer>
                                        <IconWrapper>
                                            <UsernameIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            id={'username'}
                                            type={'text'}
                                            floatingLabelText={'Username'}
                                            onChange={(e) => this.setState({ username: e.target.value })}
                                            value={this.state.username}
                                            fullWidth={true}
                                            errorText={this.state.username_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <InputContainer>
                                        <IconWrapper>
                                            <PassIcon style={this.iconStyles} />
                                        </IconWrapper>
                                        <TextField
                                            id={'password'}
                                            type={'password'}
                                            floatingLabelText={'Password'}
                                            onChange={(e) => this.setState({ password: e.target.value })}
                                            value={this.state.password}
                                            fullWidth={true}
                                            errorText={this.state.password_error}
                                            style={this.textFieldStyles}
                                        />
                                    </InputContainer>
                                    <Divider style={this.dividerStyles} />
                                    <Button
                                        type={'Submit'}
                                        label={'Submit'}
                                        fullWidth={true}
                                        style={{ marginTop: 20 }}
                                    />
                                </form>
                            </Tab>
                        </Tabs>
    
                    )
                }

            </DialogModal>
                    
        );
    
    };
    
}
    
    
const mapDispatchToProps = {
    createUser,
    fetchUser
};

function mapStateToProps({ creatingUser, fetchingUser }) {
    return {
        creatingUser,
        fetchingUser
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewAccountLoginModal);