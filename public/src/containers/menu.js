import React, { Component } from 'react';
import { connect } from 'react-redux';
import { openMenu } from '../actions/actions';
import { deepOrange500, brown500, grey300, green500, grey800, lightBlue800 } from 'material-ui/styles/colors'
import Drawer from 'material-ui/Drawer';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import PastDueIcon from 'material-ui/svg-icons/action/alarm';
import PersonalIcon from 'material-ui/svg-icons/action/home';
import WorkIcon from 'material-ui/svg-icons/action/work';
import CurrentIcon from 'material-ui/svg-icons/action/event';
import AllIcon from 'material-ui/svg-icons/action/list';
import CompletedIcon from 'material-ui/svg-icons/action/done';
import PriorityIcon from 'material-ui/svg-icons/notification/priority-high';
import ShoppingIcon from 'material-ui/svg-icons/action/shopping-cart';
import TravelIcon from 'material-ui/svg-icons/action/flight-takeoff';
import LogoutIcon from 'material-ui/svg-icons/communication/vpn-key';
import LetterAvatar from 'material-ui/Avatar';
import CategoryCount from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import styled from 'styled-components';
import Cookies from 'cookies-js';

const Email = styled.p`
    font-weight: 300;
    font-size: 0.8em;
    margin-top: 5px;
`;

const Username = styled.h4`
    font-weight: 400;
    font-size: 1.1em;
    margin-bottom: 0;
`;

const Category = styled.h4`
    font-weight: 400;
    margin: 0;
    paddingTop: 5px;
`;

class Menu extends Component {
    constructor(props){
        super(props);

        this.state = {
            username: '',
            email: ''
        };

        this.categoryStyles = {
            padding: '3px 0'
        }

        this.onDrawerStateChange = this.onDrawerStateChange.bind(this);
        this.avatar = this.avatar.bind(this);
        this.username = this.username.bind(this);
        this.email = this.email.bind(this);
        this.logout = this.logout.bind(this);

    };

    /* 
        The username and email props from database are cached, then on component mount, these props are set to this.state.
    */

    componentDidMount(){

        this.setState({ username: JSON.parse(localStorage.getItem('username')), email: JSON.parse(localStorage.getItem('email')) });

    };

    /* 
        Closes menu.
    */

    onDrawerStateChange() {

        return this.props.openMenu(false);

    };

    /*  
        Avatar function checks to see if the username prop is available from this.props. If true, returns the first letter of username prop.
        If false, checks to see if username prop has been cached, if true returns first letter of cached username prop.
        Else, returns empty string. 
    */

    avatar(){

        if(this.props.creatingUser.payload.username){

            return this.props.creatingUser.payload.username[0]

        } else if (this.props.fetchingUser.payload.username ){

            return this.props.fetchingUser.payload.username[0]

        } else if (this.state.username){

            return this.state.username[0]
        }

        return '';

    };

    /*  
        Username function checks to see if the username prop is available from this.props. If true, returns username prop.
        If false, checks to see if username prop has been cached, if true returns cached username prop.
        Else, returns empty string. 
    */

    username(){

        if(this.props.creatingUser.payload.username){

            return this.props.creatingUser.payload.username

        } else if (this.props.fetchingUser.payload.username){

            return this.props.fetchingUser.payload.username

        } else if (this.state.username){

            return this.state.username
        }

        return '';
    };

    /*  
        Email function checks to see if the email prop is available from this.props. If true, returns email prop.
        If false, checks to see if email prop has been cached, if true returns cached email prop.
        Else, returns empty string. 
    */

    email(){

        if (this.props.creatingUser.payload.email) {

            return this.props.creatingUser.payload.email

        } else if (this.props.fetchingUser.payload.email) {

            return this.props.fetchingUser.payload.email

        } else if (this.state.email) {

            return this.state.email
        }

        return '';
    };


    logout(){
        
        Cookies.expire('authenticated');
        return location.reload();
    };

    render(){

        return (

            <Drawer open={ this.props.menu.open } docked={false} onRequestChange={this.onDrawerStateChange}>

                <List style={{ padding: '0'}}>

                    {/*  User Avatar, Username, and E-mail Begin */}

                    <ListItem 
                        disabled={true} 
                        style={{ padding: '22px 16px', backgroundColor: '#1D2733' }}
                    >
                        <LetterAvatar size={60} color={'#ffffff'}>
                            {this.avatar()}
                        </LetterAvatar>

                        <Username>
                            {this.username()}
                        </Username>

                        <Email>
                            {this.email()}
                        </Email>

                    </ListItem>

                    {/*  User Avatar, Username, and E-mail End */}

                    {/*  Current Begin */}

                    <ListItem
                        leftIcon={<CurrentIcon color={'#e0a905'} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Current</Category>
                    </ListItem>

                    {/*  Current End */}

                    {/*  Past Due Begin */}

                    <ListItem
                        leftIcon={<PastDueIcon color={deepOrange500} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Past Due</Category>
                    </ListItem>

                    {/* Past Due End */}

                    {/*  All Begin */}

                    <ListItem
                        leftIcon={<AllIcon color={grey300} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>All</Category>
                    </ListItem>

                    {/*  All End */}

                    {/*  Done Begin */}

                    <ListItem
                        leftIcon={<CompletedIcon color={green500} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Completed</Category>
                    </ListItem>

                    {/*  Done End */}

                    {/*  Priority Begin */}

                    <ListItem
                        leftIcon={<PriorityIcon color={deepOrange500} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Priority</Category>
                    </ListItem>

                    {/*  Priority End */}

                    <Divider style={{ backgroundColor: grey800 }} />

                    {/*  Personal Begin */}

                    <ListItem 
                        leftIcon={<PersonalIcon color={'#e0a905'} />} 
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'} 
                                color={'#fff'} 
                                size={28} 
                                style={{ paddingTop: '7px' }}>
                                {0} 
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                            <Category>Personal</Category>
                    </ListItem>

                    {/*  Personal End */}

                    {/*  Work Begin */}

                    <ListItem
                        leftIcon={<WorkIcon color={brown500} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Work</Category>
                    </ListItem>

                    {/*  Work End */}

                    {/*  Travel Begin */}
        
                    <ListItem
                        leftIcon={<TravelIcon color={lightBlue800} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Travel</Category>
                    </ListItem>

                    {/*  Travel End */}

                    {/*  Shopping Begin */}

                    <ListItem
                        leftIcon={<ShoppingIcon color={green500} />}
                        rightAvatar={
                            <CategoryCount backgroundColor={'transparent'}
                                color={'#fff'}
                                size={28}
                                style={{ paddingTop: '7px' }}>
                                {0}
                            </CategoryCount>
                        }
                        style={this.categoryStyles}
                        >
                        <Category>Shopping</Category>
                    </ListItem>

                    {/*  Shopping End */}

                    <Divider style={{ backgroundColor: grey800 }} />

                    {/*  Logout Begin */}

                    <ListItem
                        leftIcon={<LogoutIcon color={'#e0a905'} />}
                        onClick={this.logout}
                        style={{ padding: '10px 0'}}
                    >
                        <Category>Logout</Category>
                    </ListItem>

                    {/*  Logout End */}

                </List>

            </Drawer>
        );
    };
};

const mapDispatchToProps = {
    openMenu
};

function mapStateToProps({ creatingUser, fetchingUser, menu }) {
    return {
        creatingUser,
        fetchingUser,
        menu
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);