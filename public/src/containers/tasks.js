import React, { Component }  from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { deepOrange500, brown500, grey300, green500, grey600, lightBlue700 } from 'material-ui/styles/colors';
import PastDueTasks from '../components/tasksContainer';
import CurrentTasks from '../components/tasksContainer';
import CompletedTasks from '../components/tasksContainer';
import PastDueIcon from 'material-ui/svg-icons/action/alarm';
import CurrentIcon from 'material-ui/svg-icons/action/event';
import CompletedIcon from 'material-ui/svg-icons/action/done';
import Task from '../components/task';

const TasksContainer = styled.div`
    display: inline-flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: stretch;
    width: 100%;
    height: auto;

    @media screen and (max-width: 1024px){
        display: flex;
        flex-direction: column;
    }
`;

class Tasks extends Component {
    constructor(props){
        super(props);
        this.state = {};

        this.iconStyles = {
            width: '30px',
            height: '30px',
            pastDue: deepOrange500,
            current: '#e0a905',
            completed: green500,
        };

        this.pastDue = [
            <Task key={1} name={'lorem ipsum delor lorem ipsum delor'} status={'past due'} priority={true} completed={false} category={3} hasAttachment={false} />
        ];

    };

    render(){

        return (
            <TasksContainer>

                <PastDueTasks
                    flexOrder={'1'}
                    icon={
                        <PastDueIcon 
                            style={{ 
                                width: this.iconStyles.width, 
                                height: this.iconStyles.height, 
                                color: this.iconStyles.pastDue
                            }} 
                        />
                    }
                    heading={'Past Due Tasks'}
                    id={'past_due_tasks'}
                    noExpand={JSON.parse(localStorage.getItem('past_due_tasks'))}
                >
                    {this.pastDue}
                </PastDueTasks>

                <CurrentTasks
                    flexOrder={'2'}
                    icon={
                        <CurrentIcon 
                            style={{
                                width: this.iconStyles.width,
                                height: this.iconStyles.height,
                                color: this.iconStyles.current 
                            }}
                        /> 
                    }
                    heading={'Current Tasks'}
                    id={'current_tasks'}
                    noExpand={JSON.parse(localStorage.getItem('current_tasks'))}
                >
                    <Task key={''} name={'lorem ipsum delor lorem ipsum delor'} status={'current'} priority={true} completed={false} category={2} hasAttachment={true} />
                </CurrentTasks>

                <CompletedTasks
                    flexOrder={'3'}
                    icon={
                        <CompletedIcon 
                            style={{ 
                                width: this.iconStyles.width, 
                                height: this.iconStyles.height, 
                                color: this.iconStyles.completed 
                            }} 
                        /> 
                    }
                    heading={'Completed Tasks'}
                    id={'completed_tasks'}
                    noExpand={JSON.parse(localStorage.getItem('completed_tasks'))}
                >
                    <Task key={''} name={'lorem ipsum delor lorem ipsum delor'} status={'completed'} priority={false} completed={true} category={1} hasAttachment={false} />
                </CompletedTasks>

            </TasksContainer>
        );

    }

};
        
const mapDispatchToProps = {

};

function mapStateToProps({ submittingTask }) {
    return {
        submittingTask
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
