import React, { Component } from 'react';
import { connect } from 'react-redux';
import { openCreate, submitTask } from '../actions/actions';
import styled from 'styled-components';
import { deepOrange100, deepOrange200, deepOrange500, brown500, grey300, green500, grey600, lightBlue700 } from 'material-ui/styles/colors'
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import PriorityToggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import CreateIcon from 'material-ui/svg-icons/content/create';
import FileIcon from 'material-ui/svg-icons/file/attachment';
import Spinner from '../components/spinner';
import _ from '../util';


const InputContainer = styled.div`
    height: auto;
    padding: 0px 20px;

    @media screen and (max-width: 768px){
        padding: 0px;
    }
`;

const DateAttachmentContainer = styled.div`
    display: block;
    width: fit-content;
    height: auto;

`;

const DateWrapper = styled.div`
    display: inline-block; 
    width: fit-content;
`;

const PriorityAttachementWrapper = styled.div`
    display: inline-block; 
    width: 320px; 
    max-width: 100%; 
    margin-top: 20px;
    margin-left: 82px;
    vertical-align: top;

    @media screen and (max-width: 768px){
        width: 220px;
        margin-left: 40px;
    }

    @media screen and (max-width: 470px){
        margin-left: 0px;
    }
`;

const Attachment = styled.div``;

const ClearAttachementContainer = styled.div`
    display: ${props => props.display};
    width: 32px;
    height: 32px;
    margin-left: 5px;
    transform: translateY(8px);
    cursor: pointer;
`;


class CreateTaskModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            details: '',
            priority: false,
            completed: false,
            date: new Date(),
            time: new Date(),
            category: 1,
            hasAttachment: false,
            attachment: '',
            clearIcon: 'none',
            error: 'This field is required.',
            reposition: false
        };

        this.dividerStyles = {
            backgroundColor: 'transparent',
            margin: '5px 0'
        };

        this.priorityToggleStyle = {
            thumbOff: {
                backgroundColor: deepOrange100,
            },
            trackOff: {
                backgroundColor: deepOrange200,
            },
            thumbSwitched: {
                backgroundColor: deepOrange500,
            },
            trackSwitched: {
                backgroundColor: deepOrange200,
            }
        };

        this.textFieldStyles = {
            display: 'block',
            margin: '5px 0',
            width: '100%'
        };

        this.onTaskCancel = this.onTaskCancel.bind(this);
        this.onTaskCreate = this.onTaskCreate.bind(this);
        this.clearAttachment = this.clearAttachment.bind(this);
    };

    componentDidUpdate(prevProps){
        if(prevProps.submittingTask.isSubmitting){

            this.props.openCreate(false);

            return this.setState({
                name: '',
                details: '',
                date: new Date(),
                time: new Date(),
                priority: false,
                hasAttachment: false,
                attachment: '',
                clearIcon: 'none',
            });
        }

    };

    onTaskCancel() {
        this.props.openCreate(false);
        this.attachmentInput.value = '';
        return this.setState({ 
            name: '', 
            details: '', 
            date: new Date(), 
            time: new Date(),
            priority: false,
            hasAttachment: false, 
            attachment: '', 
            clearIcon: 'none' 
        });
    };

    onTaskCreate(){
        this.setState({ reposition: true });
        return this.props.submitTask(this.state.name, this.state.details, this.state.date, this.state.time, this.state.category, this.state.priority, this.state.completed, this.state.hasAttachment, this.state.attachment);
    };

    clearAttachment(){
        this.attachmentInput.value = '';
        return this.setState({ 
            hasAttachment: false, 
            attachment: '', 
            clearIcon: 'none' 
        });
    };

    render(){

        const actions = [
            <FlatButton
                label={'Cancel'}
                onClick={this.onTaskCancel}
            />,
            <RaisedButton
                label={'Create'}
                labelPosition={'before'}
                icon={<CreateIcon style={{ width: '20px', height: '20px', marginTop: '-3px' }} />}
                disabled={this.state.name.length < 1 || this.state.details.length < 1}
                onClick={this.onTaskCreate}
            />
        ];

        return (

            <Dialog 
                modal={true} 
                open={this.props.create.open} 
                title={'Create Task'} 
                actions={actions} 
                titleStyle={{ 
                    display: this.props.submittingTask.isSubmitting ? 'none' : 'block', color: '#e0a905' 
                }}
                actionsContainerStyle={{
                    display: this.props.submittingTask.isSubmitting ? 'none' : 'block' 
                }}
                autoDetectWindowHeight={true}
                autoScrollBodyContent={true} 
                bodyStyle={{ border: 'none' }}
                repositionOnUpdate={this.state.reposition}
            >

                { this.props.submittingTask.isSubmitting ? (

                    <div style={{ 
                        transform: 'translateY(12px)' 
                    }}>
                        <Spinner />
                    </div>

                    ) : (

                        <InputContainer>

                            { /* Task Details Start */}

                            <TextField
                                name={'task_name'}
                                onChange={(e) => this.setState({ name: e.target.value })}
                                value={this.state.task_name}
                                floatingLabelText={'Task Name'}
                                floatingLabelStyle={{ color: '#e0a905' }}
                                floatingLabelFixed={true}
                                errorText={this.state.name ? '' : this.state.error}
                                errorStyle={{ color: '#e0a905' }}
                                multiLine={true}
                                rows={1}
                                rowsMax={2}
                                style={this.textFieldStyles}
                                textareaStyle={{ 
                                    backgroundColor: '#1D2733', 
                                    borderRadius: '2px', 
                                    padding: '10px 0'
                                }}
                                underlineStyle={{
                                    borderColor: 'transparent' 
                                }}
                            />

                            { /* Task Name End */}

                            <Divider style={this.dividerStyles} />

                            { /* Task Details Start */}
                        
                            <TextField
                                name={'task_details'}
                                onChange={(e) => this.setState({ details: e.target.value })}
                                value={this.state.task_details}
                                floatingLabelText={'Task Details'}
                                floatingLabelStyle={{ color: '#e0a905' }}
                                floatingLabelFixed={true}
                                errorText={this.state.details ? '' : this.state.error}
                                errorStyle={{ color: '#e0a905' }}
                                multiLine={true}
                                rows={5}
                                rowsMax={5}
                                style={this.textFieldStyles}
                                textareaStyle={{ 
                                    backgroundColor: '#1D2733', 
                                    borderRadius: '2px' 
                                }}
                                underlineStyle={{ 
                                    borderColor: 'transparent' 
                                }}
                            />

                            { /* Task Details End */}

                            <Divider style={this.dividerStyles} />

                            <DateAttachmentContainer>
                                <DateWrapper>

                                    { /* Due Date Start */}

                                    <DatePicker
                                        name={'due_date'}
                                        formatDate={(d) => {
                                            let month = _.getMonthByName(d);
                                            let day = _.getDayWithOrdinal(d);
                                            let year = d.getFullYear();
                                            return `${month} ${day}, ${year}`;
                                        }}
                                        textFieldStyle={{ cursor: 'pointer' }}
                                        inputStyle={{ paddingTop: '3px', color: '#e0a905' }}
                                        onChange={(e, d) => this.setState({ date: d })}
                                        defaultDate={this.state.date}
                                        floatingLabelText={'Due Date'}
                                    />

                                    { /* Due Date End */}

                                    { /* Due Time Start */}

                                    <TimePicker
                                        name={'due_time'}
                                        format={'ampm'}
                                        onChange={(e, t) => this.setState({ time: t })}
                                        defaultTime={this.state.time}
                                        textFieldStyle={{ cursor: 'pointer' }}
                                        inputStyle={{ paddingTop: '3px', color: '#e0a905' }}
                                        floatingLabelText={'Due Time'}
                                    />

                                    { /* Due Time End */}

                                    { /* Category Dropdown Start */}

                                    <SelectField
                                        floatingLabelText={'Category'}
                                        value={this.state.category}
                                        onChange={(e, i, v) => this.setState({ category: v })}
                                        labelStyle={{ color: '#e0a905' }}
                                        listStyle={{ backgroundColor: '#1D2733' }}
                                    >
                                        <MenuItem value={1} primaryText={'Personal'} />
                                        <MenuItem value={2} primaryText={'Work'} />
                                        <MenuItem value={3} primaryText={'Travel'} />
                                        <MenuItem value={4} primaryText={'Shopping'} />
                                    </SelectField>

                                    { /* Category Dropdown End */}

                                </DateWrapper>

                                <PriorityAttachementWrapper>

                                    { /* High Priority Toggle Start */}
                
                                    <PriorityToggle
                                        label={'High Priority?'}
                                        labelPosition={'right'}
                                        toggled={this.state.priority}
                                        onToggle={() => this.setState({ priority: !this.state.priority })}
                                        thumbStyle={this.priorityToggleStyle.thumbOff}
                                        trackStyle={this.priorityToggleStyle.trackOff}
                                        thumbSwitchedStyle={this.priorityToggleStyle.thumbSwitched}
                                        trackSwitchedStyle={this.priorityToggleStyle.trackSwitched}
                                    />

                                    { /* High Priority Toggle End */}

                                    <Divider style={{ marginTop: '25px', backgroundColor: 'transparent' }} />

                                    { /* Attachment File Input Start  */}

                                    <Attachment>
                                        <RaisedButton
                                            containerElement={'label'}
                                            label={'Attach File'}
                                            labelPosition={'before'}
                                            style={{ 
                                                display: 'block',
                                                marginRight: '10px', 
                                                maxWidth: '150px' 
                                            }}
                                            icon={<FileIcon style={{ width: '25px', height: '25px', marginTop: '-3px' }} />}
                                        >
                                            <input
                                                name="attachment" 
                                                accept=".png, .jpg, .jpeg, .doc, .docx, .pdf, application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                                                type="file"
                                                ref={(input) => { this.attachmentInput = input; }}
                                                onChange={(e) => (
                                                    this.setState({ 
                                                        hasAttachment: true, 
                                                        attachment: e.target.files[0], 
                                                        clearIcon: 'inline-block' 
                                                    }) 
                                                )} 
                                                style={{ display: 'none' }}
                                            />
                                        </RaisedButton>
                                        <h5 
                                            style={{ 
                                                display: 'inline-block', 
                                                color: '#ffffff', 
                                                fontWeight: '300', 
                                                marginBottom: '0px', 
                                                marginTop: '5px', 
                                                maxWidth: '147px', 
                                                overflowX: 'hidden' 
                                            }}
                                        >
                                            { this.state.attachment ? this.state.attachment.name : 'No file chosen.' }
                                        </h5>
                                        <ClearAttachementContainer
                                            display={this.state.clearIcon}
                                            onClick={this.clearAttachment}
                                        >
                                            <ClearIcon style={{ color: deepOrange500 }} />
                                        </ClearAttachementContainer>
                                    </Attachment>

                                    { /* Attachment File Input End  */}

                                </PriorityAttachementWrapper>
                            </DateAttachmentContainer>
                        </InputContainer>
                    )
                }

            </Dialog>

        );

    };

};

const mapDispatchToProps = {
    openCreate,
    submitTask
};

function mapStateToProps({ create, submittingTask }){
    return {
        create,
        submittingTask
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTaskModal);