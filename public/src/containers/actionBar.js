import React, { Component } from 'react';
import { connect } from 'react-redux';
import { openMenu, openCreate } from '../actions/actions';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/content/create';


class ActionBar extends Component{
    constructor(props){
        super(props);
        this.state = {};

        this.onDrawerStateChange = this.onDrawerStateChange.bind(this);
        this.onDialogStateChange = this.onDialogStateChange.bind(this);
    };

    onDrawerStateChange() {
        return this.props.openMenu(true);
    };

    onDialogStateChange(){
        return this.props.openCreate(true);
    };

    render(){
        return (
            <AppBar 
                title={'Todoo'} 
                titleStyle={{ fontWeight: 300 }}
                style={{ paddingRight: '15px' }} 
                onLeftIconButtonClick={this.onDrawerStateChange}
            >
                <IconButton 
                    tooltip={'Create Task'} 
                    style={{ width: '71px', height: '91px' }} 
                    iconStyle={{ width: '36px', height: '36px' }}
                    onClick={this.onDialogStateChange}
                >
                    <AddIcon color={'#e0a905'} />
                </IconButton>
            </AppBar>
        );
    };
};

const mapDispatchToProps = {
    openMenu,
    openCreate
};

function mapStateToProps({ creatingUser, fetchingUser }) {
    return {
        creatingUser,
        fetchingUser
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ActionBar);