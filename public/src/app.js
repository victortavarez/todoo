import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { DragDropContext } from 'react-beautiful-dnd';
import Store from './reducers/rootReducer';
import Normalize from 'normalize.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { deepOrange500, brown500, grey300, green500, grey600, lightBlue700 } from 'material-ui/styles/colors'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import ActionBar from './containers/actionBar';
import NewAccountLoginModal from './containers/newAccountLoginModal';
import Menu from './containers/menu';
import CreateTaskModal from './containers/createTaskModal';
import Tasks from './containers/tasks';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#373f46',
        primary2Color: '#1D2733',
        primary3Color: '#515c65',
        accent1Color: '#e0a905',
        accent2Color: '#e0a905',
        textColor: '#ffffff',
        canvasColor: '#2d333a'
    },
    appBar: {
        textColor: '#e0a905',
        height: 91
    },
    textField: {
        floatingLabelColor: '#ffffff',
        focusColor: '#e0a905',
        borderColor: '#ffffff',
    },
    tabs: {
        selectedTextColor: '#e0a905'
    },
    refreshIndicator: {
        loadingStrokeColor: '#ffffff'
    },
    raisedButton: {
        textColor: '#ffffff',
        color: '#e0a905',
        disabledTextColor: grey600
    },
    flatButton:{
        primaryTextColor: '#ffffff'
    },
    snackbar: {
        backgroundColor: '#000000'
    },
    avatar: {
        backgroundColor: '#e0a905'
    },
    datePicker: {
        headerColor: '#e0a905',
        selectColor: '#e0a905'
    },
    timePicker: {
        headerColor: '#e0a905',
        selectColor: '#e0a905'
    },
    checkbox: {
        checkedColor: deepOrange500
    }
});

class App extends Component {
    constructor(props){
        super(props);
        this.onDragUpdate = this.onDragUpdate.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    };

    onDragUpdate(){

    };

    onDragEnd(){

    };

    render(){

        return (

            <DragDropContext
                onDragUpdate={this.onDragUpdate}
                onDragEnd={this.onDragEnd}
            >
                <NewAccountLoginModal />
                <ActionBar />
                <Menu />
                <CreateTaskModal />
                <Tasks />
            </DragDropContext>

        );
    }

}

ReactDOM.render(
    <Provider store={Store}>
        <MuiThemeProvider muiTheme={muiTheme}>
            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
);