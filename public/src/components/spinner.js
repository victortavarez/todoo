import React from 'react';
import LoadingSpinner from 'material-ui/RefreshIndicator/';

function Spinner(props){
    return (
        <LoadingSpinner
            size={50}
            left={0}
            top={0}
            status="loading"
            style={{ display: 'block', position: 'relative', backgroundColor: '#e0a905', marginLeft: 'calc(50% - 25px)' }}
        />
    )
};

export default Spinner;