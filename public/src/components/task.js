import React from 'react';
import _ from '../util';
import styled from 'styled-components';
import { red900, red400, green500, green900, brown500, lightBlue800 } from 'material-ui/styles/colors';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import PriorityIcon from 'material-ui/svg-icons/notification/priority-high';
import MoreInfoIcon from 'material-ui/svg-icons/navigation/more-horiz';
import CompletedIcon from 'material-ui/svg-icons/action/done';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import TimeIcon from 'material-ui/svg-icons/device/access-time';
import DateIcon from 'material-ui/svg-icons/action/date-range';
import PersonalIcon from 'material-ui/svg-icons/action/home';
import WorkIcon from 'material-ui/svg-icons/action/work';
import TravelIcon from 'material-ui/svg-icons/action/flight-takeoff';
import ShoppingIcon from 'material-ui/svg-icons/action/shopping-cart';
import Attachment from 'material-ui/svg-icons/file/attachment';
import IconButton from 'material-ui/IconButton';

const currentStatus = (props) => {
    if (props.status === 'past due') return red400;
    if(props.status === 'completed') return green500;
    return '#28313a';
};

const Container = styled.div`
    position: relative;
    height: 125px;
    background-color: ${currentStatus};
    margin-top: 5px;
    border-left: 1px solid rgb(29, 39, 51);
    border-right: 1px solid rgb(29, 39, 51);
    overflow: hidden;
    white-space: nowrap;
    cursor: grab;
`;

const InfoContainer = styled.div`
    display: inline-block;
    box-sizing: border-box;
    vertical-align: top;
    width: 100%;
    max-width: calc(100% - 119px);
    height: 100%;
    max-height: 85px;
`;

const EditContainer = styled.div`
    display: inline-block;
    box-sizing: border-box;
    vertical-align: top;
    min-width: 25%;
    height: 100%;
    max-height: 85px;
`;

const EditButtonsWrapper = styled.div`
    position: relative;
    width: fit-content;
    height: auto;
`;

const NameWrapper = styled.div`
    max-height: 40px;
    white-space: nowrap;
    overflow: hidden;
    padding: 0 0 0 11px;
`;

const Name = styled.h6`
    display: inline-block;
    box-sizing: border-box;
    max-height: 40px;
    max-width: 85%;
    margin: 0px;
    padding: 10px 0;
    font-family: Roboto, sans-serif;
    font-weight: 400;
    font-size: 17px;
    color: #ffffff;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const Priority = styled.div`
    display: ${props => props.priority ? 'inline-block' : 'none'};
    vertical-align: top;
    box-sizing: border-box;
    width: 40px;
    height: 40px;
    padding: 5px;
`;

const DueDateAndTimeWrapper = styled.div`
    width: 100%;
    max-height: 40px;
    box-sizing: border-box;
    white-space: nowrap;
    overflow: hidden;
    margin-top: 5px;
    padding: 0 0 0 10px;
`;

const DateWrapper = styled.div`
    display: inline-block;
    width: fit-content;
    max-height: 40px;
    max-width: 150px;
    white-space: nowrap;
    overflow: hidden;
`;

const TimeWrapper = DateWrapper.extend``;

const Icon = styled.div`
    display: inline-block;
    vertical-align: top;
    box-sizing: border-box;
    width: 20px;
    height: 20px;
    margin-top: 8px;
    margin-right: 5px;
`;

const CategoryWrapper = styled.div`
    display: inline-block;
    max-height: 40px;
    box-sizing: border-box;
    white-space: nowrap;
    overflow: hidden;
    padding: 0 0 0 10px;
`;

const Category = styled.h6`
    display: inline-block;
    box-sizing: border-box;
    max-height: 40px;
    color: #ffffff;
    font-family: Roboto, sans-serif;
    font-weight: 400;
    font-size: 15px;
    margin-top: 14px;
    margin-bottom: 0;
`;

const CategoryIcon = Icon.extend`
    margin-right: 5px;
    margin-top: 10px;
`;

const AttachmentWrapper = CategoryWrapper.extend``;

const AttachmentTitle = Category.extend``;

const AttachmentIcon = CategoryIcon.extend``;


function Task(props){

    const styles = {
        iconSizes: {
            width: '30px',
            height: '30px'
        }
    };

    const date = new Date();

    return (
        <Container status={props.status}>
            <NameWrapper>
                <Name>
                    {props.name}
                    </Name>
                <Priority priority={props.priority}>
                    <PriorityIcon color={red900} />
                </Priority>
            </NameWrapper>
            <InfoContainer>
                <DueDateAndTimeWrapper>
                    <DateWrapper>
                        <Icon>
                            <DateIcon style={{ width: '20px', height: '20px' }} />
                        </Icon>
                        <DatePicker
                            name={'due_date'}
                            formatDate={(d) => {
                                let month = _.getMonthByName(d);
                                let day = _.getDayWithOrdinal(d);
                                let year = d.getFullYear();
                                return `${month} ${day}, ${year}`;
                            }}
                            textFieldStyle={{ cursor: 'inherit', height: '40px', width: 'fit-content' }}
                            inputStyle={{ color: '#ffffff', fontSize: '15px' }}
                            value={date}
                            underlineShow={false}
                            style={{ display: 'inline-block', maxHeight: '40px', maxWidth: '138px' }}
                            disabled={true}
                        />
                    </DateWrapper>
                    <TimeWrapper>
                        <Icon>
                            <TimeIcon style={{ width: '20px', height: '20px' }} />
                        </Icon>
                        <TimePicker
                            name={'due_time'}
                            format={'ampm'}
                            inputStyle={{ color: '#ffffff', fontSize: '15px' }}
                            textFieldStyle={{ cursor: 'inherit', height: '40px', width: 'fit-content' }}
                            value={date}
                            underlineShow={false}
                            style={{ display: 'inline-block', maxHeight: '40px', width: 'fit-content', maxWidth: '65px' }}
                            disabled={true}
                        />
                    </TimeWrapper>
                </DueDateAndTimeWrapper>
                <CategoryWrapper>
                    <CategoryIcon>
                        { props.category === 1 ? 
                            (
                                <PersonalIcon style={{ width: '22px', height: '22px' }} />

                            ) : props.category === 2 ?
                            (
                                <WorkIcon style={{ width: '20px', height: '20px' }} />

                            ) : props.category === 3 ?
                            (
                                <TravelIcon style={{ width: '20px', height: '20px' }} />

                            ) : props.category === 4 ?
                            (
                                <ShoppingIcon style={{ width: '20px', height: '20px' }} />

                            ) : null
                        }
                    </CategoryIcon>
                    <Category>
                        { props.category === 1 ? 
                            (
                                'Personal'

                            ) : props.category === 2 ?
                            (
                                'Work'

                            ) : props.category === 3 ?
                            (
                                'Travel'

                            ) : props.category === 4 ?
                            (
                                'Shopping'

                            ) : null
                        }
                    </Category>
                </CategoryWrapper>
                { props.hasAttachment ? 
                    (
                        <AttachmentWrapper>
                            <AttachmentIcon>
                                <Attachment />
                            </AttachmentIcon>
                            <AttachmentTitle>
                                Attachment
                            </AttachmentTitle>
                        </AttachmentWrapper>

                    ) : null
                }
            </InfoContainer>
            <EditContainer>
                <EditButtonsWrapper>
                    <IconButton
                        tooltip={'More Info'}
                        iconStyle={styles.iconSizes}
                    >
                        <MoreInfoIcon />
                    </IconButton>
                    { props.completed ? 
                        (
                            <IconButton
                                tooltip={'Delete'}
                                iconStyle={styles.iconSizes}
                            >
                                <DeleteIcon />
                            </IconButton>

                        ) : (

                            <IconButton
                                tooltip={'Complete'}
                                iconStyle={styles.iconSizes}
                            >
                                <CompletedIcon />
                            </IconButton>
                        )
                    }
                </EditButtonsWrapper>
            </EditContainer>
        </Container>
    );
};

export default Task;