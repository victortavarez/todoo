import React, { Component } from 'react';
import styled from 'styled-components';
import Title from './tasksHeading';

const Container = styled.div`
    order: ${props => props.order};
    flex: 0 1 33.333%;
    max-height: ${props => props.expand};
    margin-top: 10px;
    overflow: hidden; 
    transition: max-height .25s ease-in-out;
`;

class TasksContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expand: this.props.noExpand ? '40px' : '5000px'
        };

        this.expandTasks = this.expandTasks.bind(this);
    };

    expandTasks(){
        this.state.expand === '5000px' ? this.setState({ expand: '40px' }) : this.setState({ expand: '5000px'});
    };

    render(){

        return (
            <Container order={this.props.flexOrder} expand={this.state.expand} className={this.props.className}>
                <Title 
                    icon={this.props.icon} 
                    heading={this.props.heading}
                    noExpand={this.props.noExpand}
                    expandTasks={this.expandTasks}
                    id={this.props.id}
                />
                {this.props.children}
            </Container>
        );

    };

};


export default TasksContainer;