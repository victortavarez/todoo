import React from 'react';
import styled from 'styled-components';
import { deepOrange500, brown500, grey300, green500, grey600, lightBlue800 } from 'material-ui/styles/colors'
import ExpandIcon from 'material-ui/svg-icons/navigation/expand-less';

const Container = styled.div`
    width: 100%;
    height: 40px;
    padding: 5px 15px;
    box-sizing: border-box;
    background-color: ${lightBlue800};
    border-left: 1px solid rgb(29, 39, 51);
    border-right: 1px solid rgb(29, 39, 51);
`;

const HeadingWrapper = styled.div`
    display: inline-block;
    position: relative;
    width: fit-content;
    height: auto;
    margin-left: 5px;
`;

const Icon = styled.div`
    display: inline-block;
    width: 30px;
    height: 30px;
`;

const Heading = styled.h5`
    display: inline-block;
    vertical-align: top;
    font-family: Roboto, sans-serif;
    font-weight: 400;
    font-size: 18px;
    margin-left: 10px;
    margin-top: 5px;
    margin-bottom: 0px;
    margin-right: 0px;
    color: #ffffff;
`;

const ExpandWrapper = styled.div`
    display: inline-block;
    position: relative;
    width: fit-content;
    float: right;
    height: auto;
    vertical-align: top;
    margin-right: 15px;
    cursor: pointer;
`;

function Title(props){

    const expandIconAnimation = (event) =>{
        event.stopPropagation();

        if(event.target.classList.contains(props.id)){

            if (event.target.style.transform === 'rotate(180deg)') {

                localStorage.setItem(`${props.id}`, 'true');
                event.target.style.transform = '';
                return props.expandTasks();

            } else {

                localStorage.removeItem(`${props.id}`);
                event.target.style.transform = 'rotate(180deg)';
                return props.expandTasks();
            }

        };

    };


    const style = {
        icon: {
            width: '28px',
            height: '28px',
            transition: 'all .2s ease-in',
            transform: props.noExpand ? '' : 'rotate(180deg)'
        }
    };

    return (
        <Container>
            <HeadingWrapper>
                <Icon>
                    {props.icon}
                </Icon>
                <Heading>
                    {props.heading}
                </Heading>
            </HeadingWrapper>
            <ExpandWrapper
                onClick={expandIconAnimation}
            >
                <ExpandIcon
                    className={props.id}
                    style={style.icon}
                />
            </ExpandWrapper>
        </Container>
    );

};

export default Title;